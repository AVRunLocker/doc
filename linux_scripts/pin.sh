#!/bin/bash

echo "Usage: NAME PORTx 0~7";
echo "Example: ./pin.sh RST_TARGET A 7";
echo "-------------------";

NAME=$1;
PORT=PORT$2;
PIN=$3;

echo "#define "$NAME"_PORT	$PORT	// Define PORT";
echo "#define "$NAME"_PIN	$PIN	// Define pin number on PORT";
echo "";
echo "// Set "$NAME" to logical 0";
echo "#define "$NAME"_0	"$NAME"_PORT = "$NAME"_PORT & ~(1<<"$NAME"_PIN)";
echo "//Set "$NAME" to logical 1";
echo "#define "$NAME"_1	"$NAME"_PORT = "$NAME"_PORT | (1<<"$NAME"_PIN)";
echo "";
echo "";
